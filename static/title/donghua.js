(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"donghua_atlas_", frames: [[0,0,891,152],[0,154,1920,61],[893,0,140,6]]}
];


// symbols:



(lib._1 = function() {
	this.initialize(ss["donghua_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.bg = function() {
	this.initialize(ss["donghua_atlas_"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.tt = function() {
	this.initialize(ss["donghua_atlas_"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.元件_4_图层_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 图层_1
	this.instance = new lib.tt();
	this.instance.parent = this;
	this.instance.setTransform(212,-2);

	this.instance_1 = new lib.bg();
	this.instance_1.parent = this;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件_4_图层_1, null, null);


(lib.元件_1_图层_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 图层_1
	this.instance = new lib._1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件_1_图层_1, null, null);


(lib.元件4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 图层_1_obj_
	this.图层_1 = new lib.元件_4_图层_1();
	this.图层_1.name = "图层_1";
	this.图层_1.parent = this;
	this.图层_1.setTransform(960,29.5,1,1,0,0,0,960,29.5);
	this.图层_1.depth = 0;
	this.图层_1.isAttachedToCamera = 0
	this.图层_1.isAttachedToMask = 0
	this.图层_1.layerDepth = 0
	this.图层_1.layerIndex = 0
	this.图层_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.图层_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件4, new cjs.Rectangle(0,-2,1920,63), null);


(lib.元件1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 图层_1_obj_
	this.图层_1 = new lib.元件_1_图层_1();
	this.图层_1.name = "图层_1";
	this.图层_1.parent = this;
	this.图层_1.setTransform(445.5,76,1,1,0,0,0,445.5,76);
	this.图层_1.depth = 0;
	this.图层_1.isAttachedToCamera = 0
	this.图层_1.isAttachedToMask = 0
	this.图层_1.layerDepth = 0
	this.图层_1.layerIndex = 0
	this.图层_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.图层_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件1, new cjs.Rectangle(0,0,891,152), null);


(lib.元件_2_图层_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 图层_1
	this.instance = new lib.元件1();
	this.instance.parent = this;
	this.instance.setTransform(445.5,76,1,1,0,0,0,445.5,76);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:0.0781},27).to({alpha:1},24).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.元件2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_51 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(51).call(this.frame_51).wait(1));

	// 图层_1_obj_
	this.图层_1 = new lib.元件_2_图层_1();
	this.图层_1.name = "图层_1";
	this.图层_1.parent = this;
	this.图层_1.setTransform(445.5,76,1,1,0,0,0,445.5,76);
	this.图层_1.depth = 0;
	this.图层_1.isAttachedToCamera = 0
	this.图层_1.isAttachedToMask = 0
	this.图层_1.layerDepth = 0
	this.图层_1.layerIndex = 0
	this.图层_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.图层_1).wait(52));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,891,152);


(lib.元件_3_图层_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 图层_1
	this.instance = new lib.元件2();
	this.instance.parent = this;
	this.instance.setTransform(960.5,60.45,1,1,0,0,0,445.5,76);

	this.instance_1 = new lib.元件4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(960,30.5,1,1,0,0,0,960,30.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(218));

}).prototype = p = new cjs.MovieClip();


(lib.元件3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_217 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(217).call(this.frame_217).wait(1));

	// 图层_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EBKkAMeIAA47IAcAAIAAY7g");
	var mask_graphics_1 = new cjs.Graphics().p("EBIiAMeIAA47ICeAAIAAY7g");
	var mask_graphics_2 = new cjs.Graphics().p("EBGhAMeIAA47IEfAAIAAY7g");
	var mask_graphics_3 = new cjs.Graphics().p("EBEgAMeIAA47IGgAAIAAY7g");
	var mask_graphics_4 = new cjs.Graphics().p("EBCeAMeIAA47IIiAAIAAY7g");
	var mask_graphics_5 = new cjs.Graphics().p("EBAdAMeIAA47IKjAAIAAY7g");
	var mask_graphics_6 = new cjs.Graphics().p("EA+bAMeIAA47IMlAAIAAY7g");
	var mask_graphics_7 = new cjs.Graphics().p("EA8aAMeIAA47IOmAAIAAY7g");
	var mask_graphics_8 = new cjs.Graphics().p("EA6ZAMeIAA47IQnAAIAAY7g");
	var mask_graphics_9 = new cjs.Graphics().p("EA4XAMeIAA47ISpAAIAAY7g");
	var mask_graphics_10 = new cjs.Graphics().p("EA2WAMeIAA47IUqAAIAAY7g");
	var mask_graphics_11 = new cjs.Graphics().p("EA0VAMeIAA47IWrAAIAAY7g");
	var mask_graphics_12 = new cjs.Graphics().p("EAyTAMeIAA47IYtAAIAAY7g");
	var mask_graphics_13 = new cjs.Graphics().p("EAwSAMeIAA47IauAAIAAY7g");
	var mask_graphics_14 = new cjs.Graphics().p("EAuRAMeIAA47IcvAAIAAY7g");
	var mask_graphics_15 = new cjs.Graphics().p("EAsPAMeIAA47IexAAIAAY7g");
	var mask_graphics_16 = new cjs.Graphics().p("EAqOAMeIAA47MAgyAAAIAAY7g");
	var mask_graphics_17 = new cjs.Graphics().p("EAoNAMeIAA47MAizAAAIAAY7g");
	var mask_graphics_18 = new cjs.Graphics().p("EAmLAMeIAA47MAk1AAAIAAY7g");
	var mask_graphics_19 = new cjs.Graphics().p("EAkKAMeIAA47MAm2AAAIAAY7g");
	var mask_graphics_20 = new cjs.Graphics().p("EAiJAMeIAA47MAo3AAAIAAY7g");
	var mask_graphics_21 = new cjs.Graphics().p("EAgHAMeIAA47MAq5AAAIAAY7g");
	var mask_graphics_22 = new cjs.Graphics().p("AeGMeIAA47MAs6AAAIAAY7g");
	var mask_graphics_23 = new cjs.Graphics().p("AcFMeIAA47MAu7AAAIAAY7g");
	var mask_graphics_24 = new cjs.Graphics().p("AaDMeIAA47MAw9AAAIAAY7g");
	var mask_graphics_25 = new cjs.Graphics().p("AYCMeIAA47MAy+AAAIAAY7g");
	var mask_graphics_26 = new cjs.Graphics().p("AWBMeIAA47MA0/AAAIAAY7g");
	var mask_graphics_27 = new cjs.Graphics().p("AT/MeIAA47MA3BAAAIAAY7g");
	var mask_graphics_28 = new cjs.Graphics().p("AR+MeIAA47MA5CAAAIAAY7g");
	var mask_graphics_29 = new cjs.Graphics().p("AP9MeIAA47MA7DAAAIAAY7g");
	var mask_graphics_30 = new cjs.Graphics().p("AN7MeIAA47MA9FAAAIAAY7g");
	var mask_graphics_31 = new cjs.Graphics().p("AL6MeIAA47MA/GAAAIAAY7g");
	var mask_graphics_32 = new cjs.Graphics().p("AJ5MeIAA47MBBHAAAIAAY7g");
	var mask_graphics_33 = new cjs.Graphics().p("AH3MeIAA47MBDJAAAIAAY7g");
	var mask_graphics_34 = new cjs.Graphics().p("AF2MeIAA47MBFKAAAIAAY7g");
	var mask_graphics_35 = new cjs.Graphics().p("AD1MeIAA47MBHLAAAIAAY7g");
	var mask_graphics_36 = new cjs.Graphics().p("ABzMeIAA47MBJNAAAIAAY7g");
	var mask_graphics_37 = new cjs.Graphics().p("AgNMeIAA47MBLNAAAIAAY7g");
	var mask_graphics_38 = new cjs.Graphics().p("AiPMeIAA47MBNPAAAIAAY7g");
	var mask_graphics_39 = new cjs.Graphics().p("AkQMeIAA47MBPQAAAIAAY7g");
	var mask_graphics_40 = new cjs.Graphics().p("AmRMeIAA47MBRRAAAIAAY7g");
	var mask_graphics_41 = new cjs.Graphics().p("AoTMeIAA47MBTTAAAIAAY7g");
	var mask_graphics_42 = new cjs.Graphics().p("AqUMeIAA47MBVUAAAIAAY7g");
	var mask_graphics_43 = new cjs.Graphics().p("AsVMeIAA47MBXVAAAIAAY7g");
	var mask_graphics_44 = new cjs.Graphics().p("AuXMeIAA47MBZXAAAIAAY7g");
	var mask_graphics_45 = new cjs.Graphics().p("AwYMeIAA47MBbYAAAIAAY7g");
	var mask_graphics_46 = new cjs.Graphics().p("AyZMeIAA47MBdZAAAIAAY7g");
	var mask_graphics_47 = new cjs.Graphics().p("A0bMeIAA47MBfbAAAIAAY7g");
	var mask_graphics_48 = new cjs.Graphics().p("A2cMeIAA47MBhcAAAIAAY7g");
	var mask_graphics_49 = new cjs.Graphics().p("A4dMeIAA47MBjdAAAIAAY7g");
	var mask_graphics_50 = new cjs.Graphics().p("A6fMeIAA47MBlfAAAIAAY7g");
	var mask_graphics_51 = new cjs.Graphics().p("A8gMeIAA47MBngAAAIAAY7g");
	var mask_graphics_52 = new cjs.Graphics().p("A+hMeIAA47MBphAAAIAAY7g");
	var mask_graphics_53 = new cjs.Graphics().p("EggjAMeIAA47MBrjAAAIAAY7g");
	var mask_graphics_54 = new cjs.Graphics().p("EgikAMeIAA47MBtkAAAIAAY7g");
	var mask_graphics_55 = new cjs.Graphics().p("EgklAMeIAA47MBvlAAAIAAY7g");
	var mask_graphics_56 = new cjs.Graphics().p("EgmnAMeIAA47MBxnAAAIAAY7g");
	var mask_graphics_57 = new cjs.Graphics().p("EgooAMeIAA47MBzoAAAIAAY7g");
	var mask_graphics_58 = new cjs.Graphics().p("EgqpAMeIAA47MB1pAAAIAAY7g");
	var mask_graphics_59 = new cjs.Graphics().p("EgsrAMeIAA47MB3rAAAIAAY7g");
	var mask_graphics_60 = new cjs.Graphics().p("EgusAMeIAA47MB5sAAAIAAY7g");
	var mask_graphics_61 = new cjs.Graphics().p("EgwtAMeIAA47MB7tAAAIAAY7g");
	var mask_graphics_62 = new cjs.Graphics().p("EgyvAMeIAA47MB9vAAAIAAY7g");
	var mask_graphics_63 = new cjs.Graphics().p("Eg0wAMeIAA47MB/wAAAIAAY7g");
	var mask_graphics_64 = new cjs.Graphics().p("Eg2xAMeIAA47MCBxAAAIAAY7g");
	var mask_graphics_65 = new cjs.Graphics().p("Eg4zAMeIAA47MCDzAAAIAAY7g");
	var mask_graphics_66 = new cjs.Graphics().p("Eg60AMeIAA47MCF0AAAIAAY7g");
	var mask_graphics_67 = new cjs.Graphics().p("Eg82AMeIAA47MCH2AAAIAAY7g");
	var mask_graphics_68 = new cjs.Graphics().p("Eg+3AMeIAA47MCJ3AAAIAAY7g");
	var mask_graphics_69 = new cjs.Graphics().p("EhA4AMeIAA47MCL4AAAIAAY7g");
	var mask_graphics_70 = new cjs.Graphics().p("EhC6AMeIAA47MCN6AAAIAAY7g");
	var mask_graphics_71 = new cjs.Graphics().p("EhE7AMeIAA47MCP7AAAIAAY7g");
	var mask_graphics_72 = new cjs.Graphics().p("EhG8AMeIAA47MCR8AAAIAAY7g");
	var mask_graphics_73 = new cjs.Graphics().p("EhI+AMeIAA47MCT+AAAIAAY7g");
	var mask_graphics_74 = new cjs.Graphics().p("EhK/AMeIAA47MCV/AAAIAAY7g");
	var mask_graphics_179 = new cjs.Graphics().p("EhK/AMeIAA47MCV/AAAIAAY7g");
	var mask_graphics_180 = new cjs.Graphics().p("EhJCAMeIAA47MCSFAAAIAAY7g");
	var mask_graphics_181 = new cjs.Graphics().p("EhHFAMeIAA47MCOLAAAIAAY7g");
	var mask_graphics_182 = new cjs.Graphics().p("EhFJAMeIAA47MCKTAAAIAAY7g");
	var mask_graphics_183 = new cjs.Graphics().p("EhDMAMeIAA47MCGZAAAIAAY7g");
	var mask_graphics_184 = new cjs.Graphics().p("EhBPAMeIAA47MCCfAAAIAAY7g");
	var mask_graphics_185 = new cjs.Graphics().p("Eg/SAMeIAA47MB+lAAAIAAY7g");
	var mask_graphics_186 = new cjs.Graphics().p("Eg9WAMeIAA47MB6tAAAIAAY7g");
	var mask_graphics_187 = new cjs.Graphics().p("Eg7ZAMeIAA47MB2zAAAIAAY7g");
	var mask_graphics_188 = new cjs.Graphics().p("Eg5cAMeIAA47MBy5AAAIAAY7g");
	var mask_graphics_189 = new cjs.Graphics().p("Eg3fAMeIAA47MBu/AAAIAAY7g");
	var mask_graphics_190 = new cjs.Graphics().p("Eg1jAMeIAA47MBrHAAAIAAY7g");
	var mask_graphics_191 = new cjs.Graphics().p("EgzmAMeIAA47MBnNAAAIAAY7g");
	var mask_graphics_192 = new cjs.Graphics().p("EgxpAMeIAA47MBjTAAAIAAY7g");
	var mask_graphics_193 = new cjs.Graphics().p("EgvsAMeIAA47MBfZAAAIAAY7g");
	var mask_graphics_194 = new cjs.Graphics().p("EgtvAMeIAA47MBbfAAAIAAY7g");
	var mask_graphics_195 = new cjs.Graphics().p("EgrzAMeIAA47MBXnAAAIAAY7g");
	var mask_graphics_196 = new cjs.Graphics().p("Egp2AMeIAA47MBTtAAAIAAY7g");
	var mask_graphics_197 = new cjs.Graphics().p("Egn5AMeIAA47MBPzAAAIAAY7g");
	var mask_graphics_198 = new cjs.Graphics().p("Egl8AMeIAA47MBL5AAAIAAY7g");
	var mask_graphics_199 = new cjs.Graphics().p("EgkAAMeIAA47MBIBAAAIAAY7g");
	var mask_graphics_200 = new cjs.Graphics().p("EgiDAMeIAA47MBEHAAAIAAY7g");
	var mask_graphics_201 = new cjs.Graphics().p("EggGAMeIAA47MBANAAAIAAY7g");
	var mask_graphics_202 = new cjs.Graphics().p("A+JMeIAA47MA8TAAAIAAY7g");
	var mask_graphics_203 = new cjs.Graphics().p("A8NMeIAA47MA4bAAAIAAY7g");
	var mask_graphics_204 = new cjs.Graphics().p("A6QMeIAA47MA0hAAAIAAY7g");
	var mask_graphics_205 = new cjs.Graphics().p("A4TMeIAA47MAwnAAAIAAY7g");
	var mask_graphics_206 = new cjs.Graphics().p("A2WMeIAA47MAstAAAIAAY7g");
	var mask_graphics_207 = new cjs.Graphics().p("A0aMeIAA47MAo1AAAIAAY7g");
	var mask_graphics_208 = new cjs.Graphics().p("AydMeIAA47MAk7AAAIAAY7g");
	var mask_graphics_209 = new cjs.Graphics().p("AwgMeIAA47MAhBAAAIAAY7g");
	var mask_graphics_210 = new cjs.Graphics().p("AujMeIAA47IdHAAIAAY7g");
	var mask_graphics_211 = new cjs.Graphics().p("AsmMeIAA47IZNAAIAAY7g");
	var mask_graphics_212 = new cjs.Graphics().p("AqqMeIAA47IVVAAIAAY7g");
	var mask_graphics_213 = new cjs.Graphics().p("AotMeIAA47IRbAAIAAY7g");
	var mask_graphics_214 = new cjs.Graphics().p("AmwMeIAA47INhAAIAAY7g");
	var mask_graphics_215 = new cjs.Graphics().p("AkzMeIAA47IJnAAIAAY7g");
	var mask_graphics_216 = new cjs.Graphics().p("Ai3MeIAA47IFvAAIAAY7g");
	var mask_graphics_217 = new cjs.Graphics().p("Ag6MeIAA47IB1AAIAAY7g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_1,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_2,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_3,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_4,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_5,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_6,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_7,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_8,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_9,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_10,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_11,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_12,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_13,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_14,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_15,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_16,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_17,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_18,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_19,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_20,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_21,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_22,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_23,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_24,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_25,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_26,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_27,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_28,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_29,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_30,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_31,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_32,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_33,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_34,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_35,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_36,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_37,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_38,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_39,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_40,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_41,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_42,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_43,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_44,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_45,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_46,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_47,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_48,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_49,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_50,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_51,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_52,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_53,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_54,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_55,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_56,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_57,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_58,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_59,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_60,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_61,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_62,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_63,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_64,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_65,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_66,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_67,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_68,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_69,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_70,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_71,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_72,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_73,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_74,x:480,y:64.8554}).wait(105).to({graphics:mask_graphics_179,x:480,y:64.8554}).wait(1).to({graphics:mask_graphics_180,x:467.2926,y:64.8554}).wait(1).to({graphics:mask_graphics_181,x:454.5807,y:64.8554}).wait(1).to({graphics:mask_graphics_182,x:441.8687,y:64.8554}).wait(1).to({graphics:mask_graphics_183,x:429.1567,y:64.8554}).wait(1).to({graphics:mask_graphics_184,x:416.4447,y:64.8554}).wait(1).to({graphics:mask_graphics_185,x:403.7328,y:64.8554}).wait(1).to({graphics:mask_graphics_186,x:391.0208,y:64.8554}).wait(1).to({graphics:mask_graphics_187,x:378.3088,y:64.8554}).wait(1).to({graphics:mask_graphics_188,x:365.5969,y:64.8554}).wait(1).to({graphics:mask_graphics_189,x:352.8849,y:64.8554}).wait(1).to({graphics:mask_graphics_190,x:340.1729,y:64.8554}).wait(1).to({graphics:mask_graphics_191,x:327.4609,y:64.8554}).wait(1).to({graphics:mask_graphics_192,x:314.749,y:64.8554}).wait(1).to({graphics:mask_graphics_193,x:302.037,y:64.8554}).wait(1).to({graphics:mask_graphics_194,x:289.325,y:64.8554}).wait(1).to({graphics:mask_graphics_195,x:276.6131,y:64.8554}).wait(1).to({graphics:mask_graphics_196,x:263.9011,y:64.8554}).wait(1).to({graphics:mask_graphics_197,x:251.1891,y:64.8554}).wait(1).to({graphics:mask_graphics_198,x:238.4771,y:64.8554}).wait(1).to({graphics:mask_graphics_199,x:225.7652,y:64.8554}).wait(1).to({graphics:mask_graphics_200,x:213.0532,y:64.8554}).wait(1).to({graphics:mask_graphics_201,x:200.3412,y:64.8554}).wait(1).to({graphics:mask_graphics_202,x:187.6292,y:64.8554}).wait(1).to({graphics:mask_graphics_203,x:174.9173,y:64.8554}).wait(1).to({graphics:mask_graphics_204,x:162.2053,y:64.8554}).wait(1).to({graphics:mask_graphics_205,x:149.4933,y:64.8554}).wait(1).to({graphics:mask_graphics_206,x:136.7814,y:64.8554}).wait(1).to({graphics:mask_graphics_207,x:124.0694,y:64.8554}).wait(1).to({graphics:mask_graphics_208,x:111.3574,y:64.8554}).wait(1).to({graphics:mask_graphics_209,x:98.6454,y:64.8554}).wait(1).to({graphics:mask_graphics_210,x:85.9335,y:64.8554}).wait(1).to({graphics:mask_graphics_211,x:73.2215,y:64.8554}).wait(1).to({graphics:mask_graphics_212,x:60.5095,y:64.8554}).wait(1).to({graphics:mask_graphics_213,x:47.7975,y:64.8554}).wait(1).to({graphics:mask_graphics_214,x:35.0856,y:64.8554}).wait(1).to({graphics:mask_graphics_215,x:22.3736,y:64.8554}).wait(1).to({graphics:mask_graphics_216,x:9.6616,y:64.8554}).wait(1).to({graphics:mask_graphics_217,x:-3.0386,y:64.8554}).wait(1));

	// 图层_1_obj_
	this.图层_1 = new lib.元件_3_图层_1();
	this.图层_1.name = "图层_1";
	this.图层_1.parent = this;
	this.图层_1.setTransform(960,60.5,1,1,0,0,0,960,60.5);
	this.图层_1.depth = 0;
	this.图层_1.isAttachedToCamera = 0
	this.图层_1.isAttachedToMask = 0
	this.图层_1.layerDepth = 0
	this.图层_1.layerIndex = 0
	this.图层_1.maskLayerName = 0

	var maskedShapeInstanceList = [this.图层_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.图层_1).wait(218));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-15,960.1,151.5);


(lib.场景_1_bgdonghua = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// bgdonghua
	this.instance = new lib.元件3();
	this.instance.parent = this;
	this.instance.setTransform(960,46.5,1,1,0,0,180,960,30.5);

	this.instance_1 = new lib.元件3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(960,46.5,1,1,0,0,0,960,30.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.场景_1_bgdonghua, null, null);


// stage content:
(lib.donghua = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// bgdonghua_obj_
	this.bgdonghua = new lib.场景_1_bgdonghua();
	this.bgdonghua.name = "bgdonghua";
	this.bgdonghua.parent = this;
	this.bgdonghua.setTransform(960,80.5,1,1,0,0,0,960,80.5);
	this.bgdonghua.depth = 0;
	this.bgdonghua.isAttachedToCamera = 0
	this.bgdonghua.isAttachedToMask = 0
	this.bgdonghua.layerDepth = 0
	this.bgdonghua.layerIndex = 0
	this.bgdonghua.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bgdonghua).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(960,80.5,960,80.19999999999999);
// library properties:
lib.properties = {
	id: '1BE63D8EA5CD0E429B2B53B1D443B2AA',
	width: 1920,
	height: 160,
	fps: 24,
	color: "#FFFFFF",
	opacity: 0.00,
	manifest: [
		{src:"images/donghua_atlas_.png?1607071100495", id:"donghua_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['1BE63D8EA5CD0E429B2B53B1D443B2AA'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;