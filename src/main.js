// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import echarts from 'echarts'
Vue.prototype.$echarts = echarts

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import scroll from 'vue-seamless-scroll'


import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css';
import 'swiper/dist/js/swiper';
import dataV from '@jiaminghi/data-view'


Vue.use(dataV)
Vue.use(VueAwesomeSwiper)
Vue.use(scroll)
Vue.use(ElementUI)


// 引用axios，并设置基础URL为后端服务api地址
var axios = require('axios')
axios.defaults.baseURL = '/api'
// 将API方法绑定到全局
Vue.prototype.$axios = axios

Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = false

window.zdw = function zdw(arg) {
  vueInstance.imgUrl = vueInstance.imgPre + arg;
  vueInstance.dialogVisible = true
  console.log(arg, 'arg');
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
