import Vue from 'vue';
import Router from 'vue-router';

import CityManagementMap from '../views/cloud/CityManagementMap';
import OrgManagement from '../views/cloud/OrgManagement';

import AutoSociety from '../views/cloud/AutoSociety';
import clever_environment from '../views/cloud/clever_environment';
import important_engineering from '../views/cloud/important_engineering';
import social_treatment from '../views/cloud/social_treatment';
import SyntheticApplication from '../views/cloud/SyntheticApplication';
import PeaceSite from '../views/cloud/PeaceSite';
import EmergencyManagement from '../views/cloud/EmergencyManagement';
import IntelligenceCommunity from '../views/cloud/IntelligenceCommunity';
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/CityManagementMap',
      name: 'CityManagementMap',
      component: CityManagementMap
    },
    {
      path: '/org',
      name: 'OrgManagement',
      component: OrgManagement
    },
    {
      path: '/auto',
      name: 'AutoSociety',
      component: AutoSociety
    },
    {
      path: '/clever_environment',
      name: 'clever_environment',
      component:clever_environment
    },
    {
      path: '/social_treatment',
      name: 'social_treatment',
      component:social_treatment
    },
    {
      path: '/important_engineering',
      name: 'important_engineering',
      component: important_engineering
    },
    {
      path: '/',
      name: 'SyntheticApplication',
      component: SyntheticApplication
    },
    {
      path: '/PeaceSite',
      name: 'PeaceSite',
      component: PeaceSite
    },
    {
      path: '/EmergencyManagement',
      name: 'EmergencyManagement',
      component: EmergencyManagement
    },
    {
      path: '/IntelligenceCommunity',
      name: 'IntelligenceCommunity',
      component: IntelligenceCommunity
    },
  ]


})
